FROM node

WORKDIR /data

COPY install.sh /install.sh
RUN bash /install.sh
RUN rm /install.sh

CMD ["node-red", "-u", "/data"]